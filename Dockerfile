FROM python:3-alpine
ENV PYTHONUNBUFFERED 1

WORKDIR /mysite
COPY . /mysite
RUN pip install -r requirements.txt

#RUN mkdir db

EXPOSE 8000

#VOLUME db2

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000

#https://www.youtube.com/watch?v=wwyugYWEn2Q
#2:13:51
