#!/bin/sh

# User credentials
user=admin
email=admin@example.com
password=pass

file=db/db.sqlite3
create_admin=false 

echo "init.sh >>>"

#До миграции, проверить, есть ли БД. Если ее нет, нужно создать суперпользователя
if [ -f "$file" ]; then
    echo "DB exist: $file"
else    
    create_admin=true 
    echo "DB not exist: ${file}"
fi

echo "Migration..."
python3 manage.py migrate
echo "...done"


if [ "$create_admin" = true ]; then
    echo "Create superuser..."
    echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell
    echo "...done"    
fi


echo "<<<init.sh"